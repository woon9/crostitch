//声明常量doc，优化性能
var doc = document;
//定义tableTrData作为tableTr子组件的data，控制页面显示
var tableTrData = { borderWidth: "0px", borderColor: "#808080", bgColorCol1: "#D3D3D3", bgColorCol0: "#FFFFFF" };
//存储取自服务器的初始化数据大小，用于在页面初始化时显示加载百分比
var initBlockSize;
//用于存储当前表格的缩放级别(原使用zoom属性，在360等浏览器中兼容性不好，改为transform属性)
var tableZoom = 1;
//设置允许创建的最大区块坐标乘积
var maxBlockNumber = 16;
//添加page一级参数
var pageX = 0, pageY = 0;
//对于notice，存在多次点击时重复计时的问题，故此处跳出程序定义noticeTimeoutObj，以免运行程序时重新定义obj与重置计时产生冲突
var noticeTimeoutObj, noticeTimeout = 2000;
//声明对象state，作为Vue实例的data选项，以实现数据自动相应
var state = {
	basePx: getPageRem(),
	userUsedColor: getUserSet(),
	//通过v-if判断noticeShow的结果控制显示/隐藏notice
	noticeShow: false,
	//notice显示内容
	noticeInfo: "",
	//页面方块数据
	blockNumberObj: {},
}
/*************** 颜色选择器储值 ***************/
var colorChooseDataHSV = {H : 190, S : 92, V : 83},
	  colorChooseDataRGB = {R : 15, G : 179, B : 214},
	  colorChooseDataHEX = "0FB3D6";
//多个元素需要改变颜色时，设置此变量用于存储区别
var colorChange = "";
//颜色选择器常用颜色数量
var colorChooseUsuallyBlock = 24;
/*************** 颜色选择器储值完成 ***************/