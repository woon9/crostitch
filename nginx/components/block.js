//bigBlock组件，用于显示表格
Vue.component('bigBlock', {
	template:
		`<table class="blockTable" :id="uid" style="border:none">
				<tableTr v-for="(row, key) in numbersObj" :cols="row" :uid="uid" :rowIndex="key" :coor="coordinate"/>
		</table>`,
	props: ['uid', 'coordinate', 'numbersObj'],
	mounted() {
		this.setPosition()
	},
	updated() {
		this.setPosition()
	},
	methods: {
		setPosition() {
			doc.getElementById(this.uid).style.top = this.coordinate[1] * 9.7 + 'rem';
			doc.getElementById(this.uid).style.left = this.coordinate[0] * 9.7 + 'rem';
		},
	},
})

//bigBlock组件的二次循环分组件
Vue.component('tableTr', {
	template:
		`<tr>
			<td :style="{'border-top-width': borderWidth, 'border-left-width': borderWidth, 'border-color': borderColor, 'background-color': test(col), }" v-for="(col, i) in cols" class="tableTd" @click="blockClick(col, i, cols, uid, rowIndex, coor)"></td>
		</tr>`,
	props: ['cols', 'uid', 'rowIndex', 'coor'],
	data() {
		return tableTrData
	},
	methods: {
		test(col) {
			let newcol = "#" + col;
			newcol = newcol.substring(1);
			return newcol;
		},
		blockClick(col, i, cols, uid, rowIndex, coor) {
			let changeColor;
			if (col == this.bgColorCol1) {
				changeColor = this.bgColorCol0;
			} else {
				changeColor = this.bgColorCol1;
			}
			state.blockNumberObj[uid].numbersObj[rowIndex].splice(i, 1, changeColor);
			let type = "clickBlock", page = [pageX, pageY], content = [pageX, pageY, coor[0], coor[1], rowIndex, i, changeColor];
			sendWebsocket(type, page, content);
		},
	},
})

//setBlock组件，在页面右侧设置一个可以控制一些功能的块
Vue.component('setBlock', {
	template:
		`<div id="setBlockDiv">
			<img src="pictures/enlarge.png" class="setButton setButtonNomal" title="放大" @click="enlarge" />
			<img src="pictures/narrow.png" class="setButton setButtonNomal" title="缩小" @click="narrow" />
			<img src="pictures/recovery.png" class="setButton setButtonNomal" title="恢复" @click="recovery" />
			<img src="pictures/addRow.png" class="setButton setButtonMiddle" title="增加一列" @click="addRow" />
			<img src="pictures/addLine.png" class="setButton setButtonMiddle" title="增加一行" @click="addLine" />
			<img src="pictures/redRow.png" class="setButton setButtonMiddle" title="减少一列" @click="reduceRow" />
			<img src="pictures/redLine.png" class="setButton setButtonMiddle" title="减少一行" @click="reduceLine" />
			<img src="pictures/bgColorCol0.png" class="setButton setButtonNomal" title="亮色" @click="changeBgColor0" />
			<img src="pictures/bgColorCol1.png" class="setButton setButtonNomal" title="底色" @click="changeBgColor1" />
			<img src="pictures/borderColor.png" class="setButton setButtonLittle" title="边框颜色" @click="changeBorder" />
			<img src="pictures/border.png" class="setButton setButtonLittle" title="边框" @click="border" />
		</div>`,
	methods: {
		enlarge() {
			this.$emit('enlarge')
		},
		narrow() {
			this.$emit('narrow')
		},
		recovery() {
			this.$emit('recovery')
		},
		addRow() {
			this.$emit('addRow')
		},
		addLine() {
			this.$emit('addLine')
		},
		reduceRow() {
			this.$emit('reduceRow')
		},
		reduceLine() {
			this.$emit('reduceLine')
		},
		changeBgColor1() {
			this.$emit('changeBg1')
		},
		changeBgColor0() {
			this.$emit('changeBg0')
		},
		changeBorder() {
			this.$emit('changeBorder')
		},
		border() {
			this.$emit('border')
		},
	},
})

//colorChoose组件，颜色选择器
Vue.component('colorChoose', {
	template:
`<div id="colorChoose" style="display: none">
	<!-- 明度、饱和度选择块 -->
	<div id="colorBlockBox">
		<div id="colorBlock" onmousedown="colorBlockOnMouseDown(event)"></div>
		<img id="colorBlockMouse" src="pictures/colorChooseCross.png" />
	</div>
	<div id="colorChooseBPart">
		<!-- 色相选择块 -->
		<div id="newHueBox">
			<div id="newHueBlock" onmousedown="newHueBlockOnMouseDown(event)"></div>
			<img id="newHueBlockMouse" src="pictures/colorChooseSlip.png" />
		</div>
		<!-- 颜色预览和数据显示块 -->
		<div id="newColorData">
			<div id="newColorBlock"></div>
			<div id="newDataBlock">
				<div id="newHSV" class="newDataRow">
					<ul class="newHSVARGBul">
						<li class="newHSVARGBli"><span>H:</span><input class="newHSVARGBliinput" id="newHSVARGBliinputH" type="text" maxlength="3" autocomplete="off" oninput="newHSVARGBliinputHInput(this, value)" value="190" />°</li>
						<li class="newHSVARGBli"><span>S:</span><input class="newHSVARGBliinput" id="newHSVARGBliinputS" type="text" maxlength="3" autocomplete="off" oninput="newHSVARGBliinputSInput(this, value)" value="92" />%</li>
						<li class="newHSVARGBli"><span>V:</span><input class="newHSVARGBliinput" id="newHSVARGBliinputV" type="text" maxlength="3" autocomplete="off" oninput="newHSVARGBliinputVInput(this, value)" value="83" />%</li>
					</ul>
				</div>
				<div id="newRGB" class="newDataRow">
					<ul class="newHSVARGBul">
						<li class="newHSVARGBli"><span>R:</span><input class="newHSVARGBliinput" id="newHSVARGBliinputR" type="text" maxlength="3" autocomplete="off" oninput="newHSVARGBliinputRInput(this, value)" value="15" /></li>
						<li class="newHSVARGBli"><span>G:</span><input class="newHSVARGBliinput" id="newHSVARGBliinputG" type="text" maxlength="3" autocomplete="off" oninput="newHSVARGBliinputGInput(this, value)" value="179" /></li>
						<li class="newHSVARGBli"><span>B:</span><input class="newHSVARGBliinput" id="newHSVARGBliinputB" type="text" maxlength="3" autocomplete="off" oninput="newHSVARGBliinputBInput(this, value)" value="214" /></li>
					</ul>
				</div>
				<div id="newHEX" class="newDataRow">
					<li class="newHSVARGBli" id="newHSVARGBliHEX"><span id="newHSVARGBliHEXSpan1">HEX:</span><span id="newHSVARGBliHEXSpan2">#</span><input class="newHSVARGBliinput" id="newHSVARGBliinputHEX" type="text" maxlength="6" autocomplete="off" oninput="newHSVARGBliinputHEXInput(this, value)" value="0FB3D6" /></li>
					<li class="newHSVARGBli" id="newHSVARGBliConfirm">
						<img id="colorChooseConfirm" src="pictures/colorChooseConfirm.png" onclick="colorChooseConfirm()" />
						<img id="colorChooseCancel" src="pictures/colorChooseCancel.png" onclick="colorChooseCancel()" />
					</li>
				</div>
			</div>
		</div>
		<!-- 常用颜色选择块 -->
		<div id="colorChooseUsually">
			<div class="clCsUsuallyLittleBlock">
				<ul>
					<li id="clCsUsuallyLittleBlockLi10" onclick="colorChooseUsuallyClick('10')"></li>
					<li id="clCsUsuallyLittleBlockLi11" onclick="colorChooseUsuallyClick('11')"></li>
					<li id="clCsUsuallyLittleBlockLi12" onclick="colorChooseUsuallyClick('12')"></li>
					<li id="clCsUsuallyLittleBlockLi13" onclick="colorChooseUsuallyClick('13')"></li>
					<li id="clCsUsuallyLittleBlockLi14" onclick="colorChooseUsuallyClick('14')"></li>
					<li id="clCsUsuallyLittleBlockLi15" onclick="colorChooseUsuallyClick('15')"></li>
					<li id="clCsUsuallyLittleBlockLi16" onclick="colorChooseUsuallyClick('16')"></li>
					<li id="clCsUsuallyLittleBlockLi17" onclick="colorChooseUsuallyClick('17')"></li>
				</ul>
				<ul>
					<li id="clCsUsuallyLittleBlockLi18" onclick="colorChooseUsuallyClick('18')"></li>
					<li id="clCsUsuallyLittleBlockLi19" onclick="colorChooseUsuallyClick('19')"></li>
					<li id="clCsUsuallyLittleBlockLi20" onclick="colorChooseUsuallyClick('20')"></li>
					<li id="clCsUsuallyLittleBlockLi21" onclick="colorChooseUsuallyClick('21')"></li>
					<li id="clCsUsuallyLittleBlockLi22" onclick="colorChooseUsuallyClick('22')"></li>
					<li id="clCsUsuallyLittleBlockLi23" onclick="colorChooseUsuallyClick('23')"></li>
					<li id="clCsUsuallyLittleBlockLi24" onclick="colorChooseUsuallyClick('24')"></li>
					<li id="clCsUsuallyLittleBlockLi25" onclick="colorChooseUsuallyClick('25')"></li>
				</ul>
				<ul>
					<li id="clCsUsuallyLittleBlockLi26" onclick="colorChooseUsuallyClick('26')"></li>
					<li id="clCsUsuallyLittleBlockLi27" onclick="colorChooseUsuallyClick('27')"></li>
					<li id="clCsUsuallyLittleBlockLi28" onclick="colorChooseUsuallyClick('28')"></li>
					<li id="clCsUsuallyLittleBlockLi29" onclick="colorChooseUsuallyClick('29')"></li>
					<li id="clCsUsuallyLittleBlockLi30" onclick="colorChooseUsuallyClick('30')"></li>
					<li id="clCsUsuallyLittleBlockLi31" onclick="colorChooseUsuallyClick('31')"></li>
					<li id="clCsUsuallyLittleBlockLi32" onclick="colorChooseUsuallyClick('32')"></li>
					<li id="clCsUsuallyLittleBlockLi33" onclick="colorChooseUsuallyClick('33')"></li>
				</ul>
			</div>
		</div>
	</div>
</div>`,
})