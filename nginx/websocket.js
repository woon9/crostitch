var websocket,
    websocketPath = "ws://crostitch.youzai.plus/crows",
    websocketFame = "BlueFame",
    websocketToken = new Date().getTime();
var websocketUrl = websocketPath + "?fame=" + websocketFame + "&token=" + websocketToken;
var reconnectWebsocketLock = false,
    websocketTimeInterval;

//建立websocket连接，在beforeCreate()中调用
function connectWebsocket(url) {
	console.log('******V1.1.1 建立websocket连接******');
	if ("WebSocket" in window) {
		try {
			websocket = new WebSocket(url);
			//执行事件函数后才能监听行为
			websocketEvent();
		} catch(err) {
			console.log('创建连接失败，catch:' + err);
			//调用重连函数
			reconnectWebsocket(url);
		}
	} else {
		alert('浏览器不支持websocket');
	}
}

//事件处理函数
function websocketEvent() {
	//建立websocket连接回调函数
	websocket.onopen = function(event) {
		//建立连接后即启动心跳检测
		websocketHeartCheck.start();
		noticeShow('成功与服务器建立连接');
		console.log('******连接成功建立，开始心跳******');
	}
	//接收到服务器数据回调函数
	websocket.onmessage = function(event) {
		//收到来自服务器的信息后，即重置心跳检测倒计时
		websocketHeartCheck.reset();
		console.log('******触发接收服务器数据处理函数******');
		let message = JSON.parse(event.data);
		let type = message.type, content = message.content;
		// console.log('接收到来自服务器的' + type + '类型信息：' + event.data);
		switch(type) {
			case "clickBlock":
				let uid = "u" + (content[2] + 10).toString() + (content[3] + 10).toString();
				// state.blockNumberObj[uid].numbersObj[content[4]].splice(content[5], 1, content[6]);
				Vue.set(state.blockNumberObj[uid].numbersObj[content[4]], content[5], content[6]);
				// console.log('type-' + message.type + '已执行处理');
				// vm.$forceUpdate();
				break;
				
			case "changeBgColor":
				//取页面当前背景色作为待替换颜色，此方式可以防止页面背景色失步时响应其它页面改变背景色失败
				let oldColor = tableTrData.bgColorCol1;
				state.blockNumberObj = JSON.parse(JSON.stringify(state.blockNumberObj).replace(new RegExp(oldColor, "g"), content));
				tableTrData.bgColorCol1 = content;
				noticeShow('背景颜色已改变');
				// console.log('type-' + message.type + '已执行处理');
				break;
				
			case "changeBdColor":
				tableTrData.borderColor = content;
				noticeShow('边框颜色已改变');
				// console.log('type-' + message.type + '已执行处理');
				break;
				
			case "addBlock":
				let newDateBlock = {};
				for (i = 0; i < content[0].length; i++) {
					let key = content[0][i];
					let value = {uid: key, coordinate: content[1][i], numbersObj: getStandardBlock()}
					//使用Vue.set(object, propertyName, value)方法向嵌套对象添加响应式属性(不适用此方法新增的区块点击不能响应)
					Vue.set(state.blockNumberObj, key, value);
				}
				noticeShow('区块已增加');
				// console.log('type-' + message.type + '已执行处理');
				break;
				
			case "reduceBlock":
				let blockNumbersArr = [], blockNbObjNameArr = Object.keys(state.blockNumberObj);
				for (i = 0; i < blockNbObjNameArr.length; i++) {
					let splice = (({uid, coordinate}) =>({uid, coordinate}))(state.blockNumberObj[blockNbObjNameArr[i]]);
					blockNumbersArr.push(splice);
				}
				let deleteUid = [];
				for (i2 = 0; i2 < Object.keys(content).length; i2++) {
					let found = blockNumbersArr.find(element => (element.coordinate[0] == content[i2][0]) && (element.coordinate[1] == content[i2][1]));
					if (found) {
						deleteUid.push(found.uid);
					}
				}
				for (i3 = 0; i3 < deleteUid.length; i3++) {
					delete state.blockNumberObj[deleteUid[i3]];
				}
				//主要用作更新视图，顺带提示
				noticeShow('区块已删除');
				// console.log('type-' + message.type + '已执行处理');
				break;
				
			case "heartCheck":
				console.log(content);
				break;
				
			default:
				break;
		}
	}
	//websocket关闭回调函数
	websocket.onclose = function(event) {
		console.log('******触发websocket关闭数据处理函数******');
		//确认连接状态
		if (websocket.readyState != 1) {
			reconnectWebsocket(websocketUrl);
		}
	}
	//websocket通信错误回调函数
	websocket.onerror = function(event) {
		console.log('******触发通信错误处理函数******');
		//确认连接状态
		if (websocket.readyState != 1) {
			reconnectWebsocket(websocketUrl);
		}
	}
	//监听窗口关闭事件，防止窗口关闭时未断开websocket连接，抛出异常; window.onbeforeunload在即将离开当前页面(刷新或关闭)时执行
	window.onbeforeunload = function() {
		websocket.close();
	}
}

//重建websocket连接函数
function reconnectWebsocket(url) {
	console.log('******触发重建websocket连接函数******');
	if (reconnectWebsocketLock) {
		return;
	} else {
		reconnectWebsocketLock = true;
	}
	let timeout = 5000;
	websocketTimeInterval && clearTimeout(websocketTimeInterval);
	websocketTimeInterval = setTimeout(function() {
		noticeShow('已与服务器断开，正在重新连接...');
		reconnectWebsocketLock = false;
		connectWebsocket(url);
	}, timeout);
}

//websocket心跳检测
var websocketHeartCheck = {
	timeout: 60000,
	timeoutObj: null,
	severTimeoutObj: null,
	reset: function() {
		clearTimeout(this.timeoutObj);
		clearTimeout(this.severTimeoutObj);
		this.start();
	},
	start: function() {
		let self = this;
		this.timeoutObj && clearTimeout(this.timeoutObj);
		this.severTimeoutObj && clearTimeout(this.severTimeoutObj);
		this.timeoutObj = setTimeout(function() {
			let message = {
				type: "heartCheck",
				content: "Pongbong"
			}
			websocket.send(JSON.stringify(message));
			self.severTimeoutObj = setTimeout(function() {
				websocket.close();
			}, 30000)
		}, this.timeout)
	}
}

//websocket发送数据函数
function sendWebsocket(type, page, content) {
	console.log('******触发主动发送数据处理函数******');
	let message = {
		"type": type,
		"page" : page,
		"content": content
	}
	websocket.send(JSON.stringify(message));
}