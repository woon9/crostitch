//向服务器取初始化数据大小、背景颜色和边框颜色
function getInitInfo() {
	axios({
		method: 'post',
		url: 'http://crostitch.youzai.plus/ask/getInitInfo',
		data:  {'pageX': pageX, 'pageY': pageY},
	})
	.then(function (response) {
		initBlockSize = response.data.dataSize / 2;
		if (response.data.bgColor) {
			tableTrData.bgColorCol1 = response.data.bgColor;
		}
		if (response.data.bdColor) {
			tableTrData.borderColor = response.data.bdColor;
		}
	})
	.catch(function (error) {
		console.log('获取初始化数据出现错误，catch: ' + error);
	});
}

//页面初始化，通过axios取出页面数据赋给state.blockNumberObj对象
function getServerData() {
	axios({
		method: 'post',
		url: 'http://crostitch.youzai.plus/ask/getPage',
		data: {},
		//获取下载进度，并以百分比形式呈现在页面上
		onDownloadProgress(event) {
			let progressNow = Math.round(event.loaded / initBlockSize * 100);
			if (progressNow > 100) {
				progressNow = 100;
			}
			let notice = "正在加载数据 " + progressNow + "%";
			noticeShow(notice);
		}
	})
	.then(function (response) {
		// console.log(response.headers.datasize);
		let serverData = response.data.page;
		let coorArray = Object.keys(serverData);
		let localData = {};
		for (let i = 0; i < coorArray.length; i++) {
			let coorElement = coorArray[i].split("@"),
			coorX = Number(coorElement[0]),
			coorY = Number(coorElement[1]);
			let block = {
				uid: "u" + (coorX + 10).toString() + (coorY + 10).toString(),
				coordinate: [coorX, coorY],
				numbersObj: serverData[coorArray[i]]
			}
			let newUid = block.uid;
			localData[newUid] = block;
		}
		state.blockNumberObj = localData;
	})
	.catch(function (error) {
		console.log('获取页面数据出现错误，catch: ' + error);
	});
}

// /*************************单机测试*************************/
// //使用本地存储来暂代后台数据传出，取出的数据赋给state.blockNumberObj对象，在main.js生命周期钩子beforeCreate()中调用
// function getLocalStorage() {
// 	if (localStorage.getItem('blockData') == null ) {
// 		const testData = {
// 			'u1010' : {
// 				uid: 'u1010',
// 				coordinate: [0,0],
// 				//调用getStandardBlock()函数生成的标准块作为numbersObj
// 				numbersObj: getStandardBlock(),
// 			}
// 		}
// 		//将名为blockData实为testData的对象通过JSON.stringify()方法转换为字符串存入本地存储，本地存储不能直接存入对象
// 		localStorage.setItem('blockData',JSON.stringify(testData));
// 	}
// 	//获取本地存储中名为blockData的JSON字符串并通过JSON.parse()方法转换为对象
// 	state.blockNumberObj = JSON.parse(localStorage.getItem('blockData'));
// }

//修改背景颜色处理函数，在颜色选择器的colorChooseConfirm()函数中调用
function changeBackgroundColor(newColor) {
	let type = "changeBgColor", page = [pageX, pageY], content = newColor;
	sendWebsocket(type, page, content);
}

//修改边框颜色
function changeBoderColor(color) {
	let type = "changeBdColor", page = [pageX, pageY], content = color;
	sendWebsocket(type, page, content);
}

//设置html的fontSize大小，用于rem，自适应页面docElement
function getPageRem() {
	basePx = doc.documentElement.clientWidth / 10 + 'px';
	doc.documentElement.style.fontSize = basePx;
	return basePx;
}

//循环生成标准块数据
function getStandardBlock() {
	let maxColARow = 64, standardBlock = [], colArray = [], colElement = "#D3D3D3";
	for (let row = 0; row < maxColARow; row++) {
		for (let col = 0; col < maxColARow; col++) {
			colArray[col] = colElement;
		}
		standardBlock[row] = colArray;
	}
	//此步骤非常重要，通过多此一举的转换，使standardBlock真正拥有了每一个值; 转换之前，在点击刚循环生成的区块时同一列格子会全部响应
	standardBlock = JSON.parse(JSON.stringify(standardBlock).replace(new RegExp("#D3D3D3", "g"), tableTrData.bgColorCol1));
	return standardBlock;
}

//读取存储在localStorage中的用户习惯数据(所选颜色、边框设置、常用颜色)，若无相关数据则写入标准数据
function getUserSet() {
	if (localStorage.getItem('userSelectedColor') == null ) {
		const userSelectedColor = "#FFFFFF";
		localStorage.setItem('userSelectedColor',JSON.stringify(userSelectedColor));
	}
	if (localStorage.getItem('userBoderWidth') == null ) {
		const userBoderWidth = "0px";
		localStorage.setItem('userBoderWidth',JSON.stringify(userBoderWidth));
	}
	if (localStorage.getItem('userUsedColor') == null ) {
		const userUsedColor = ["#4169E1", "#1E90FF", "#00BFFF", "#00CED1", "#90EE90", "#3CB371", "#228B22", "#006400", "#FFE4B5", "#FFD700", "#FF8C00", "#FF6347", "#DC143C", "#FF0000", "#FF99CC", "#FFCCCC", "#000000", "#808080", "#D3D3D3", "#FFFFFF", "#FFFF00", "#FFA500", "#0000FF", "#800080"]
		localStorage.setItem('userUsedColor',JSON.stringify(userUsedColor));
	}
	let userSelectedColor = JSON.parse(localStorage.getItem('userSelectedColor'));
	let userBoderWidth = JSON.parse(localStorage.getItem('userBoderWidth'));
	let userUsedColor = JSON.parse(localStorage.getItem('userUsedColor'));
	tableTrData.bgColorCol0 = userSelectedColor;
	tableTrData.borderWidth = userBoderWidth;
	return userUsedColor;
}

//用于设置id为bigBlockBox的元素的transform属性的scale值，实现点击按钮页面放大/缩小
function setBigBlockTrs(zoom) {
	doc.getElementById("bigBlockBox").style.transform = 'scale(' + zoom + ')';
	doc.getElementById("bigBlockBox").style.OTransform = 'scale(' + zoom + ')';
	doc.getElementById("bigBlockBox").style.MsTransform = 'scale(' + zoom + ')';
	doc.getElementById("bigBlockBox").style.MozTransform = 'scale(' + zoom + ')';
	doc.getElementById("bigBlockBox").style.webkitTransform = 'scale(' + zoom + ')';
}

//控制notice的显示与隐藏
function noticeShow(text) {
	if (state.noticeShow = true) {
		clearTimeout(noticeTimeoutObj);
	}
	state.noticeShow = true;
	state.noticeInfo = text;
	noticeTimeoutObj = setTimeout(function() {
		state.noticeShow = false;
		state.noticeInfo = "";
	}, noticeTimeout)
}

/************************************************ 增加一列/一行 ************************************************/
//增加相应方块到数据中
function addBlock(newDate) {
	//newDate = [newUid, newXY]
	let type = "addBlock", page = [pageX, pageY], content = newDate;
	sendWebsocket(type, page, content);
}

//当页面方块完全删除时，无法直接调用函数增加，此处定义一个直接增加的函数
function addOneBlock() {
	let newDate = [], newUid = [], newXY = [], minX = 0, minY = 0, Xstr, Ystr;
	newXY[0] = [minX, minY];
	Xstr = (10 + minX).toString();
	Ystr = (10 + minY).toString();
	newUid[0] = 'u' + Xstr + Ystr;
	newDate = [newUid, newXY];
	addBlock(newDate);
}

//获取增加一列时新的Uid和坐标值
function getNewRowUidXY(maxX, maxY) {
	let newDate = [], newUid = [], newXY = [], X = maxX + 1, Y, Xstr, Ystr;
	Xstr = (10 + X).toString();
	for (i = 0; i <= maxY; i++) {
		Y = i;
		Ystr = (10 + Y).toString();
		newXY[i] = [X, Y];
		newUid[i] = 'u' + Xstr + Ystr;
	}
	return newDate = [newUid, newXY];
}

//获取增加一行时新的Uid和坐标值
function getNewLineUidXY(maxX, maxY) {
	let newDate = [], newUid = [], newXY = [], X, Y = maxY + 1, Xstr, Ystr;
	Ystr = (10 + Y).toString();
	for (i = 0; i <= maxX; i++) {
		X = i;
		Xstr = (10 + X).toString();
		newXY[i] = [X, Y];
		newUid[i] = 'u' + Xstr + Ystr;
	}
	return newDate = [newUid, newXY];
}

//遍历现有数据，获取已使用uid，坐标最大值
function getUsedUidXY() {
	let bkNbOKey = Object.keys(state.blockNumberObj);
	let coordinateXY = [], coordinateX = [], coordinateY = [], maxX, maxY;
	for (let i = 0; i < bkNbOKey.length; i++) {
		coordinateXY[i] = state.blockNumberObj[(bkNbOKey[i])].coordinate;
		coordinateX.push(coordinateXY[i][0]);
		coordinateY.push(coordinateXY[i][1]);
	}
	coordinateX.sort(function (a, b) {
		return a - b;
	})
	coordinateY.sort(function (a, b) {
		return a - b;
	})
	maxX = coordinateX[coordinateXY.length-1];
	maxY = coordinateY[coordinateXY.length-1];
	return {maxX: maxX, maxY: maxY, usedUid: bkNbOKey};
}

/************************************************ 减少一列/一行 ************************************************/
//根据要减少的坐标删去相应Block
function reduceBlock(reduceXY) {
	//reduceXY = [[x, y],[x, y], ...]
	let type = "reduceBlock", page = [pageX, pageY], content = reduceXY;
	sendWebsocket(type, page, content);
}

//获取减少一列时的坐标值
function getReduceRowXY(maxX, maxY) {
	let reduceXY = [];
	for (i = 0; i <= maxY; i++) {
		reduceXY[i] = [maxX, i];
	}
	if (reduceXY.length == 0) {
		noticeShow('当前所有区块已删除');
	} else {
		noticeShow('正在删除方块，请稍等');
		reduceBlock(reduceXY);
	}
}

//获取减少一行时的坐标值
function getReduceLineXY(maxX, maxY) {
	let reduceXY = [];
	for (i = 0; i <= maxX; i++) {
		reduceXY[i] = [i, maxY];
	}
	if (reduceXY.length == 0) {
		noticeShow('当前所有区块已删除');
	} else {
		noticeShow('正在删除方块，请稍等');
		reduceBlock(reduceXY);
	}
}

/************************************************ 颜色选择器JS部分 ************************************************/
	function callColorChoose(color, caseX, positionTop, positionRight, positionBottom, positionLeft) {
		colorChange = caseX;
		let colorChooseDisplay = document.getElementById('colorChoose');
		if (colorChooseDisplay.style.display == 'none') {
			colorChooseDisplay.style.display = 'block';
		} else if (colorChooseDisplay.style.display == 'block') {
			colorChooseDisplay.style.display = 'none';
		}
		doc.getElementById("colorChoose").style.top = positionTop;
		doc.getElementById("colorChoose").style.right = positionRight;
		doc.getElementById("colorChoose").style.bottom = positionBottom;
		doc.getElementById("colorChoose").style.left = positionLeft;
		let regHex = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
		let regRgb = /^(rgb|RGB)/;
		if (regHex.test(color)) {
			colorChooseDataHEX = color.toUpperCase().replace(/#/, "");
			colorChooseDataRGB = hexToRgb(colorChooseDataHEX);
			colorChooseDataHSV = rgbToHsv(colorChooseDataRGB);
		} else if (regRgb.test(color)) {
			let colorArr = color.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",");
			colorChooseDataRGB = {R : Number(colorArr[0]), G : Number(colorArr[1]), B : Number(colorArr[2])};
			colorChooseDataHEX = rgbToHex(colorChooseDataRGB);
			colorChooseDataHSV = rgbToHsv(colorChooseDataRGB);
		} else {
			console.log('当前颜色格式非RGB或HEX格式，不显示当前颜色');
		}
		setColorChooseHSVInput();
		setColorChooseRGBInput();
		setColorChooseHEXInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function colorChooseConfirm() {
		let newColor = '#' + colorChooseDataHEX;
		colorChoosePutUsedColor(newColor);
		switch (Number(colorChange.substr(4,1))) {
			case 1:
				let oldColor = tableTrData.bgColorCol1;
				if (oldColor.toUpperCase() == newColor.toUpperCase()) {
					break;
				} else {
					changeBackgroundColor(newColor);
					break;
				}
			case 2:
				tableTrData.bgColorCol0 = newColor;
				localStorage.setItem('userSelectedColor', JSON.stringify(newColor));
				break;
			case 3:
				changeBoderColor(newColor);
				break;
		}
		doc.getElementById("colorChoose").style.display = 'none';
		return;
	}
	
	function colorChooseCancel() {
		doc.getElementById("colorChoose").style.display = 'none';
		return;
	}
	
	function setClBkMuBgNHueBkMs(obj) {
		let h = obj.H, s = obj.S, v = obj.V;
		newHueBlockMouse.style.left = Math.round(h * 389 / 360) + 'px';
		colorBlockMouse.style.left = Math.round(s * 4 - 7) + 'px';
		colorBlockMouse.style.top = Math.round(0 - 2.2 * v - 11) + 'px';
		if (s < 25 && v > 82) {
			colorBlockMouse.style.filter = "contrast(0%)";
		} else {
			colorBlockMouse.style.filter = "contrast(100%)";
		}
		doc.getElementById("colorBlock").style.background = 'hsl(' + h + ', 100%, 50%)';
		let rgb = hsvToRgb(obj);
		doc.getElementById("newColorBlock").style.background = 'rgb(' + rgb.R + ',' + rgb.G + ',' + rgb.B + ')';
	}
	
	function setColorChooseHSVInput() {
		let newHSVARGBliinputH = doc.getElementById("newHSVARGBliinputH");
		let newHSVARGBliinputS = doc.getElementById("newHSVARGBliinputS");
		let newHSVARGBliinputV = doc.getElementById("newHSVARGBliinputV");
		newHSVARGBliinputH.value = colorChooseDataHSV.H;
		newHSVARGBliinputS.value = colorChooseDataHSV.S;
		newHSVARGBliinputV.value = colorChooseDataHSV.V;
	}
	
	function setColorChooseRGBInput() {
		let newHSVARGBliinputR = doc.getElementById("newHSVARGBliinputR");
		let newHSVARGBliinputG = doc.getElementById("newHSVARGBliinputG");
		let newHSVARGBliinputB = doc.getElementById("newHSVARGBliinputB");
		newHSVARGBliinputR.value = colorChooseDataRGB.R;
		newHSVARGBliinputG.value = colorChooseDataRGB.G;
		newHSVARGBliinputB.value = colorChooseDataRGB.B;
	}
	
	function setColorChooseHEXInput() {
		let newHSVARGBliinputHEX = doc.getElementById("newHSVARGBliinputHEX");
		newHSVARGBliinputHEX.value = colorChooseDataHEX;
	}
	
	function colorChooseUsuallyClick(string) {
		let index = string - 10;
		colorChooseDataHEX = state.userUsedColor[index].toUpperCase().replace(/#/, "");
		colorChooseDataRGB = hexToRgb(colorChooseDataHEX);
		colorChooseDataHSV = rgbToHsv(colorChooseDataRGB);
		setColorChooseHEXInput();
		setColorChooseHSVInput();
		setColorChooseRGBInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function colorChooseUsuallyUsed() {
		for (let i = 0; i < colorChooseUsuallyBlock; i++) {
			let id = "clCsUsuallyLittleBlockLi" + (10 + i);
			doc.getElementById(id).style.background = state.userUsedColor[i];
		}
	}
	
	function colorChoosePutUsedColor(color) {
		let checkColor = state.userUsedColor.indexOf(color);
		let oldUsedColor = state.userUsedColor.concat();
		if (checkColor == -1) {
			for (let index = 0; index < colorChooseUsuallyBlock - 1; index++) {
				state.userUsedColor[index + 1] = oldUsedColor[index];
			}
		} else {
			for (let index = 0; index < checkColor; index++) {
				state.userUsedColor[index + 1] = oldUsedColor[index];
			}
		}
		state.userUsedColor[0] = color;
		localStorage.setItem('userUsedColor',JSON.stringify(state.userUsedColor));
		colorChooseUsuallyUsed();
	}
	
	function hsvToRgb(obj) {
		let h = obj.H, s = obj.S, v = obj.V;
		if (h == 360) {
			h = 0;
		}
		s = s / 100;
		v = v / 100;
		let r = 0, g = 0, b = 0;
		let i = parseInt((h / 60) % 6);
		let f = h / 60 - i;
		let p = v * (1 - s);
		let q = v * (1 - f * s);
		let t = v * (1 - (1 - f) * s);
		switch (i) {
			case 0:
				r = v; g = t; b = p;
			break;
			case 1:
				r = q; g = v; b = p;
			break;
			case 2:
				r = p; g = v; b = t;
			break;
			case 3:
				r = p; g = q; b = v;
			break;
			case 4:
				r = t; g = p; b = v;
			break;
			case 5:
				r = v; g = p; b = q;
			break;
			default:
				console.log('颜色选择器hsvToRgb(obj)函数取模运算结果有误，值为' + i);
			break;
		}
		r = parseInt(r * 255.0)
		g = parseInt(g * 255.0)
		b = parseInt(b * 255.0)
		return {R : r, G : g, B : b};
	}
	
	function rgbToHsv(obj) {
		let r = obj.R, g = obj.G, b = obj.B;
		let h = 0, s = 0, v = 0;
		let arr = [r, g, b];
		arr.sort(function (a, b) {
			return a - b;
		})
		let max = arr[2]
		let min = arr[0];
		v = max / 255;
		if (max === 0) {
			s = 0;
		} else {
			s = 1 - (min / max);
		}
		if (max === min) {
			h = 0;
		} else if (max === r && g >= b) {
			h = 60 * ((g - b) / (max - min)) + 0;
		} else if (max === r && g < b) {
			h = 60 * ((g - b) / (max - min)) + 360
		} else if (max === g) {
			h = 60 * ((b - r) / (max - min)) + 120
		} else if (max === b) {
			h = 60 * ((r - g) / (max - min)) + 240
		}
		h = parseInt(h);
		s = parseInt(s * 100);
		v = parseInt(v * 100);
		return {H : h, S : s, V : v};
	}
	
	function rgbToHex(obj) {
		let color = "RGB(" + obj.R + "," + obj.G + "," + obj.B + ")";
		let reg = /^(rgb|RGB)/;
		if (reg.test(color)) {
			let strHex = "";
			let colorArr = color.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",");
			for (let i = 0; i < colorArr.length; i++) {
				let hex = Number(colorArr[i]).toString(16);
				if (hex === "0") {
					hex += hex;
				} else if (hex.length < 2) {
					hex = '0' + hex;
				}
				strHex += hex;
			}
			return strHex.toUpperCase();
		} else {
			console.log('输入的RGB色有误');
			return String(color);
		}
	}
	
	function hexToRgb(string) {
		let color = '#' + string.toLowerCase();
		let reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
		if (reg.test(color)) {
			if (color.length === 4) {
				let colorNew = "#";
				for (let i = 1; i < 4; i += 1) {
					colorNew += color.slice(i, i + 1).concat(color.slice(i, i + 1));
				}
				color = colorNew;
			}
			let colorChange = [];
			for (let i = 1; i < 7; i += 2) {
				colorChange.push(parseInt("0x" + color.slice(i, i + 2)));
			}
			return {R : colorChange[0], G : colorChange[1], B : colorChange[2]};
		} else {
			console.log('输入的HEX色有误');
			return color;
		}
	}
	
	function colorChooseStddEvent(event) {
		if (!event) {
			event = window.event;
			event.target = event.srcElement;
		}
		if (event.layerX || event.layerY) {
			event.offsetX = event.layerX;
			event.offsetY = event.layerY;
		}
		event.nowXVal = event.offsetX;
		event.nowYVal = event.offsetY;
		return event;
	}

	function colorBlockOnMouseDown(event) {
		let colorBlock = doc.getElementById("colorBlock");
		let colorBlockMouse = doc.getElementById('colorBlockMouse');
		event = colorChooseStddEvent(event);
		setClBkMuPostion(event);
		colorBlock.onmousemove = clCsClBkMsMove;
		doc.onmouseup = clCsClBkMsStop;
	}
	
	function setClBkMuPostion(event) {
		let newHSVARGBliinputS = doc.getElementById("newHSVARGBliinputS");
		let newHSVARGBliinputV = doc.getElementById("newHSVARGBliinputV");
		event = colorChooseStddEvent(event);
		colorBlockMouse.style.left = (event.nowXVal - 7) + 'px';
		colorBlockMouse.style.top = (event.nowYVal - 231) + 'px';
		if (event.nowXVal < 100 && event.nowYVal < 40) {
			colorBlockMouse.style.filter = "contrast(0%)";
		} else {
			colorBlockMouse.style.filter = "contrast(100%)";
		}
		colorChooseDataHSV.S = Math.round(event.nowXVal / 400 * 100);
		colorChooseDataHSV.V = Math.round((220 - event.nowYVal) / 220 * 100);
		newHSVARGBliinputS.value = colorChooseDataHSV.S;
		newHSVARGBliinputV.value = colorChooseDataHSV.V;
		colorChooseDataRGB = hsvToRgb(colorChooseDataHSV);
		colorChooseDataHEX = rgbToHex(colorChooseDataRGB);
		setColorChooseRGBInput();
		setColorChooseHEXInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function clCsClBkMsMove(event) {
		event = colorChooseStddEvent(event);
		setClBkMuPostion(event);
	}
	
	function clCsClBkMsStop(event) {
		event = colorChooseStddEvent(event);
		doc.onmouseup = colorBlock.onmousemove = colorBlock.onmouseup = null;
	}
	
	function newHueBlockOnMouseDown(event) {
		let newHueBlock = doc.getElementById("newHueBlock");
		let newHueBlockMouse = doc.getElementById('newHueBlockMouse');
		event = colorChooseStddEvent(event);
		setNHueBkMsPostion(event);
		newHueBlock.onmousemove = clCsNHueBkMsMove;
		doc.onmouseup = clCsNHueBkMsStop;
	}
	
	function setNHueBkMsPostion(event) {
		let newHSVARGBliinputH = doc.getElementById("newHSVARGBliinputH");
		event = colorChooseStddEvent(event);
		let nHueBkMsPostion = event.nowXVal - 4.5;
		if (event.nowXVal <= 4) {
			nHueBkMsPostion = 0;
		} else if (event.nowXVal >= 393) {
			nHueBkMsPostion = 389;
		}
		newHueBlockMouse.style.left = nHueBkMsPostion + 'px';
		colorChooseDataHSV.H = Math.round(nHueBkMsPostion / 389 * 360);
		newHSVARGBliinputH.value = colorChooseDataHSV.H;
		colorChooseDataRGB = hsvToRgb(colorChooseDataHSV);
		colorChooseDataHEX = rgbToHex(colorChooseDataRGB);
		setColorChooseRGBInput();
		setColorChooseHEXInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function clCsNHueBkMsMove(event) {
		event = colorChooseStddEvent(event);
		setNHueBkMsPostion(event);
	}
	
	function clCsNHueBkMsStop(event) {
		event = colorChooseStddEvent(event);
		doc.onmouseup = newHueBlock.onmousemove = newHueBlock.onmouseup = null;
	}
	
	function newHSVARGBliinputDataTest(target, value, number) {
		target.value = value.replace(/\D/g,'');
		if (value > number) {
			target.value = number;
		}
		return target.value;
	}
	
	function newHSVARGBliinputHInput(target, value) {
		let checkedValue = newHSVARGBliinputDataTest(target, value, 360);
		colorChooseDataHSV.H = Number(checkedValue);
		colorChooseDataRGB = hsvToRgb(colorChooseDataHSV);
		colorChooseDataHEX = rgbToHex(colorChooseDataRGB);
		setColorChooseRGBInput();
		setColorChooseHEXInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function newHSVARGBliinputSInput(target, value) {
		let checkedValue = newHSVARGBliinputDataTest(target, value, 100);
		colorChooseDataHSV.S = Number(checkedValue);
		colorChooseDataRGB = hsvToRgb(colorChooseDataHSV);
		colorChooseDataHEX = rgbToHex(colorChooseDataRGB);
		setColorChooseRGBInput();
		setColorChooseHEXInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function newHSVARGBliinputVInput(target, value) {
		let checkedValue = newHSVARGBliinputDataTest(target, value, 100);
		colorChooseDataHSV.V = Number(checkedValue);
		colorChooseDataRGB = hsvToRgb(colorChooseDataHSV);
		colorChooseDataHEX = rgbToHex(colorChooseDataRGB);
		setColorChooseRGBInput();
		setColorChooseHEXInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function newHSVARGBliinputRInput(target, value) {
		let checkedValue = newHSVARGBliinputDataTest(target, value, 255);
		colorChooseDataRGB.R = Number(checkedValue);
		colorChooseDataHSV = rgbToHsv(colorChooseDataRGB);
		colorChooseDataHEX = rgbToHex(colorChooseDataRGB);
		setColorChooseHSVInput();
		setColorChooseHEXInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function newHSVARGBliinputGInput(target, value) {
		let checkedValue = newHSVARGBliinputDataTest(target, value, 255);
		colorChooseDataRGB.G = Number(checkedValue);
		colorChooseDataHSV = rgbToHsv(colorChooseDataRGB);
		colorChooseDataHEX = rgbToHex(colorChooseDataRGB);
		setColorChooseHSVInput();
		setColorChooseHEXInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function newHSVARGBliinputBInput(target, value) {
		let checkedValue = newHSVARGBliinputDataTest(target, value, 255);
		colorChooseDataRGB.B = Number(checkedValue);
		colorChooseDataHSV = rgbToHsv(colorChooseDataRGB);
		colorChooseDataHEX = rgbToHex(colorChooseDataRGB);
		setColorChooseHSVInput();
		setColorChooseHEXInput();
		setClBkMuBgNHueBkMs(colorChooseDataHSV);
	}
	
	function newHSVARGBliinputHEXInput(target, value) {
		target.value = value.replace(/[^0-9a-fA-F]/g,'').toUpperCase();
		let checkedValue = target.value;
		if (checkedValue.length == 3 || checkedValue.length == 6) {
			colorChooseDataHEX = checkedValue;
			colorChooseDataRGB = hexToRgb(colorChooseDataHEX);
			colorChooseDataHSV = rgbToHsv(colorChooseDataRGB);
			setColorChooseHSVInput();
			setColorChooseRGBInput();
			setClBkMuBgNHueBkMs(colorChooseDataHSV);
		}
	}
/************************************************ 颜色选择器JS部分结束 ************************************************/