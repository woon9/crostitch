var vm = new Vue({
	name: 'crostitch',
	el: '#app',
	data: state,

	template:
		`<div id="#app">
			<transition name="notice">
				<p id="notice" v-if="noticeShow">{{ noticeInfo }}</p>
			</transition>
			<div id="setBox">
				<setBlock @enlarge="enlargeZoom" @narrow="narrowZoom" @recovery="recoveryZoom" @border="setBorder" @addRow="clickAddBlockRow" @addLine="clickAddBlockLine" @reduceRow="clickReduceBlockRow" @reduceLine="clickReduceBlockLine" @changeBg1="changeBgColor1" @changeBg0="changeBgColor0" @changeBorder="changeBorderColor" />
				<colorChoose />
			</div>
			<div id="tableBox">
				<div id="bigBlockBox">
					<bigBlock v-for="little in blockNumberObj" :uid="little.uid" :coordinate="little.coordinate" :numbersObj="little.numbersObj" />
				</div>
			</div>
		</div>`,

	methods: {
		//通过设置id为bigBlockBox的元素的transform属性，实现缩小和放大效果，此处当点击“放大”按钮时增加transform的scale值
		enlargeZoom() {
			tableZoom = tableZoom + 0.1;
			if (tableZoom > 2.01) {
				tableZoom = 2;
				noticeShow('已放大到最高级别');
				return;
			} else {
				setBigBlockTrs(tableZoom);
			}
		},
		
		//当点击“缩小”按钮时减小transform的scale值
		narrowZoom() {
			tableZoom = tableZoom - 0.1;
			if (tableZoom < 0.09) {
				tableZoom = 0.1;
				noticeShow('已缩小到最低级别');
				return;
			} else {
				setBigBlockTrs(tableZoom);
			}
		},
		
		//当点击“恢复”按钮时恢复transform的scale值
		recoveryZoom() {
			tableZoom = 1;
			setBigBlockTrs(tableZoom);
		},
		
		//点击实现小方块的边框显示/消失
		setBorder() {
			if (tableTrData.borderWidth == "1px") {
				tableTrData.borderWidth = "0px";
			} else {
				tableTrData.borderWidth = "1px";
			}
			localStorage.setItem('userBoderWidth', JSON.stringify(tableTrData.borderWidth));
		},
		
		//增加一列
		clickAddBlockRow() {
			getStandardBlock();
			let nowData = getUsedUidXY();
			if (nowData.usedUid.length == 0) {
				addOneBlock();
			} else if ((nowData.maxX + 1) * nowData.maxY > maxBlockNumber) {
				noticeShow('将超出数量上限，创建失败');
			} else {
				noticeShow('正在增加方块，请稍等');
				let newDate = getNewRowUidXY(nowData.maxX, nowData.maxY);
				addBlock(newDate);
			}
		},
		
		//增加一行
		clickAddBlockLine() {
			getStandardBlock();
			let nowData = getUsedUidXY();
			if (nowData.usedUid.length == 0) {
				addOneBlock();
			} else if (nowData.maxX * (nowData.maxY + 1) > maxBlockNumber) {
				noticeShow('将超出数量上限，创建失败');
			} else {
				noticeShow('正在增加方块，请稍等');
				let newDate = getNewLineUidXY(nowData.maxX, nowData.maxY);
				addBlock(newDate);
			}
		},
		
		//减少一列
		clickReduceBlockRow() {
			let nowData = getUsedUidXY();
			getReduceRowXY(nowData.maxX, nowData.maxY);
		},
		
		//减少一行
		clickReduceBlockLine() {
			let nowData = getUsedUidXY();
			getReduceLineXY(nowData.maxX, nowData.maxY);
		},
		
		//呼出颜色选择器，改变点击颜色
		changeBgColor0() {
			let color = tableTrData.bgColorCol0;
			callColorChoose(color, 'case2', '30px', '100%', 'positionBottom', 'positionLeft');
		},
		
		//呼出颜色选择器，改变背景颜色
		changeBgColor1() {
			let color = tableTrData.bgColorCol1;
			callColorChoose(color, 'case1', '90px', '100%', 'positionBottom', 'positionLeft');
		},
		
		//呼出颜色选择器，改变边框颜色
		changeBorderColor() {
			let color = tableTrData.borderColor;
			callColorChoose(color, 'case3', '150px', '100%', 'positionBottom', 'positionLeft');
		},
	},
	
	beforeCreate() {
		//获取页面rem
		getPageRem(),
		//获取初始化数据总量、背景颜色和边框颜色
		getInitInfo(),
		//获取页面初始化数据
		getServerData(),
		//获取用户设置
		getUserSet(),
		//建立websocket
		connectWebsocket(websocketUrl),
		//建立标准块
		getStandardBlock()
		// 本地存储，测试用
		// getLocalStorage(),
	},
	
	mounted() {
		colorChooseUsuallyUsed()
	}
})

//监听浏览器窗口大小变化并重设basePx
window.addEventListener('resize', () => {
	state.basePx = getPageRem()
})