package youzai.crostitch.domain;

import youzai.crostitch.Constants.Constants;
import youzai.crostitch.Utils.CoordinateUtil;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@DependsOn("crosRedis")
@Component
public class CrosTool {
    public static Map getPage(int pageX, int pageY) {
        Map map = new ConcurrentHashMap();
        Set<String> pageLayouts = CrosRedis.getPageSet(pageX, pageY);
        if (pageLayouts != null && !pageLayouts.isEmpty()) {
            for (String blockLayout : pageLayouts) {
                String blockSite = blockLayout.substring(blockLayout.indexOf("k") + 1);
                String[] site = blockSite.split(Constants.COR_SPLIT);
                map.put(blockSite, CrosTool.getBlock(pageX, pageY, Integer.parseInt(site[0]), Integer.parseInt(site[1])));
                if (!map.containsKey(Constants.BG_COLOR)) {
                    String[] colors = CrosTool.getBlockColors(pageX, pageY, Integer.parseInt(site[0]), Integer.parseInt(site[1]));
                    map.put(Constants.BG_COLOR, colors[0]);
                    map.put(Constants.BD_COLOR, colors[1]);
                }
            }
        }
        return map;
    }

    public static boolean createPage(int pageX, int pageY) {
        return CrosRedis.createPage(pageX, pageY);
    }

    public static boolean deletePage(int pageX, int pageY) {
        return CrosRedis.deletePage(pageX, pageY);
    }

    public static String[] getPageColors(int pageX, int pageY) {
        String[] colors = null;
        Set<String> pageLayouts = CrosRedis.getPageSet(pageX, pageY);
        if (pageLayouts != null && !pageLayouts.isEmpty()) {
            for (String blockLayout : pageLayouts) {
                String blockSite = blockLayout.substring(blockLayout.indexOf("k") + 1);
                String[] site = blockSite.split(Constants.COR_SPLIT);
                colors = CrosTool.getBlockColors(pageX, pageY, Integer.parseInt(site[0]), Integer.parseInt(site[1]));
                break;
            }
        }
        return colors;
    }

    public static boolean changePageColor(int pageX, int pageY, String bgColor, String bdColor) {
        boolean isOk = false;
        Set<String> pageLayouts = CrosRedis.getPageSet(pageX, pageY);
        if (pageLayouts != null && !pageLayouts.isEmpty()) {
            for (String blockLayout : pageLayouts) {
                String blockSite = blockLayout.substring(blockLayout.indexOf("k") + 1);
                String[] site = blockSite.split(Constants.COR_SPLIT);
                CrosTool.changeBlockColor(pageX, pageY, Integer.parseInt(site[0]), Integer.parseInt(site[1]), bgColor, bdColor);
            }
        }

        return isOk;
    }

    public static boolean changeBlockColor(int pageX, int pageY, int blockX, int blockY, String bgColor, String bdColor) {
        return CrosRedis.changeBlockColor(pageX, pageY, blockX, blockY, bgColor, bdColor);
    }

    public static boolean hasBlock(int pageX, int pageY, int blockX, int blockY) {
        Set<String> pageLayouts = CrosRedis.getPageSet(pageX, pageY);
        return pageLayouts.contains(CoordinateUtil.redisKey(pageX, pageY, blockX, blockY));
    }

    public static String[] getBlockColors(int pageX, int pageY, int blockX, int blockY) {
        String[] colors = new String[2];
        Map<String, String> blockMap = CrosRedis.getBlock(pageX, pageY, blockX, blockY);
        colors[0] = blockMap.get(Constants.BG_COLOR);
        colors[1] = blockMap.get(Constants.BD_COLOR);
        return colors;
    }

    public static String[][] getBlock(int pageX, int pageY, int blockX, int blockY) {
        String[][] cells = new String[64][64];
        Map<String, String> blockMap = CrosRedis.getBlock(pageX, pageY, blockX, blockY);
        for (int i = 0; i < Constants.CELL_ROW; i++) {
            for (int j = 0; j < Constants.CELL_COL; j++) {
                cells[i][j] = blockMap.get(CoordinateUtil.hashKey(i, j));
            }
        }
        return cells;
    }

    public static boolean createBlock(int pageX, int pageY, int blockX, int blockY) {
        return CrosRedis.createBlock(pageX, pageY, blockX, blockY, Constants.BG_COLOR_INIT_VALUE, Constants.BD_COLOR_INIT_VALUE);
    }

    public static boolean createBlock(int pageX, int pageY, int blockX, int blockY, String bgColor, String bdColor) {
        return CrosRedis.createBlock(pageX, pageY, blockX, blockY, bgColor, bdColor);
    }

    public static boolean deleteBlock(int pageX, int pageY, int blockX, int blockY) {
        return CrosRedis.deleteBlock(pageX, pageY, blockX, blockY);
    }

    public static void makeCell(int pageX, int pageY, int blockX, int blockY, int cellX, int cellY, String color) {
        CrosRedis.makeCell(pageX, pageY, blockX, blockY, cellX, cellY, color);
    }

}
