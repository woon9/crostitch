package youzai.crostitch.domain;

import youzai.crostitch.Constants.Constants;
import youzai.crostitch.Utils.CoordinateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CrosRedis {

    @Autowired
    private static RedisTemplate redisTemplate;

    @Autowired
    public void setCrosRedis(RedisTemplate redisTemplate) {
        CrosRedis.redisTemplate = redisTemplate;
    }


    public static boolean createPage(int pageX, int pageY) {
//        for (int x = 0; x < Constants.PAGE_ROW; x++) {
//            for (int y = 0; y < Constants.PAGE_COL; y++) {
        for (int i = 0; i < Constants.BLOCK_ROW; i++) {
            for (int j = 0; j < Constants.BLOCK_COL; j++) {
                CrosRedis.createBlock(pageX, pageY, i, j, Constants.BG_COLOR_INIT_VALUE, Constants.BD_COLOR_INIT_VALUE);
            }
        }
//            }
//        }
        return true;
    }

    public static Set<String> getPageSet(int pageX, int pageY) {
        String pageLayout = CoordinateUtil.redisKey(pageX, pageY);
        return redisTemplate.boundZSetOps(pageLayout).range(0, Constants.PAGE_SIZE);
    }

    public static boolean deletePage(int pageX, int pageY) {
        boolean isOk = false;
        String pageLayout = CoordinateUtil.redisKey(pageX, pageY);
        Set<String> blockLayouts = redisTemplate.boundZSetOps(pageLayout).range(0, Constants.PAGE_SIZE);
        isOk = redisTemplate.delete(pageLayout);
        if (isOk) {
            redisTemplate.delete(blockLayouts);
        }
        return isOk;
    }

    public static Map<String, String> getBlock(int pageX, int pageY, int blockX, int blockY) {
        Map<String, String> map = null;
        String pageLayout = CoordinateUtil.redisKey(pageX, pageY);
        String blockLayout = CoordinateUtil.redisKey(pageX, pageY, blockX, blockY);
        if (redisTemplate.opsForZSet().score(pageLayout, blockLayout) != null) {
            map = redisTemplate.opsForHash().entries(blockLayout);
        }
        return map;
    }

    public static boolean changeBlockColor(int pageX, int pageY, int blockX, int blockY, String bgColor, String bdColor) {
        boolean isOk = false;
        String pageLayout = CoordinateUtil.redisKey(pageX, pageY);
        String blockLayout = CoordinateUtil.redisKey(pageX, pageY, blockX, blockY);
        if (redisTemplate.opsForZSet().score(pageLayout, blockLayout) != null) {
            if (!StringUtils.isEmpty(bgColor)) {
                BoundHashOperations bop = redisTemplate.boundHashOps(blockLayout);
                String oldBgColor = (String)bop.get(Constants.BG_COLOR);
                bop.put(Constants.BG_COLOR, bgColor);
                for (Object entry  : bop.entries().entrySet()){
                    Map.Entry<String,String> eMap = (Map.Entry<String,String>)entry;
                    if (eMap.getValue().equals(oldBgColor)) {
                        bop.put(eMap.getKey(),bgColor);
                    }
                }
            }
            if (!StringUtils.isEmpty(bdColor)) {
                redisTemplate.opsForHash().put(blockLayout, Constants.BD_COLOR, bdColor);
            }
        }
        return isOk;
    }

    public static boolean deleteBlock(int pageX, int pageY, int blockX, int blockY) {
        boolean isOk = false;
        String pageLayout = CoordinateUtil.redisKey(pageX, pageY);
        String blockLayout = CoordinateUtil.redisKey(pageX, pageY, blockX, blockY);
        if (redisTemplate.opsForZSet().remove(pageLayout, blockLayout) > 0) {
            isOk = redisTemplate.delete(blockLayout);
        }
        return isOk;
    }

    public static boolean createBlock(int pageX, int pageY, int blockX, int blockY, String bgColor, String bdColor) {
        boolean isOk = false;
        String pageLayout = CoordinateUtil.redisKey(pageX, pageY);
        String blockLayout = CoordinateUtil.redisKey(pageX, pageY, blockX, blockY);
        isOk = redisTemplate.opsForZSet().add(pageLayout, blockLayout, Constants.REDIS_ZSET_SCORE);
        if (isOk) {
            Map<String, String> map = new ConcurrentHashMap<>();
            for (int i = 0; i < Constants.CELL_ROW; i++) {
                for (int j = 0; j < Constants.CELL_COL; j++) {
                    map.put(CoordinateUtil.hashKey(i, j), bgColor);
                }
            }
            map.put(Constants.BG_COLOR, bgColor);
            map.put(Constants.BD_COLOR, bdColor);
            redisTemplate.opsForHash().putAll(blockLayout, map);
        }
        return isOk;
    }

    public static void removeCell(int pageX, int pageY, int blockX, int blockY, int cellX, int cellY) {
        String blockLayout = CoordinateUtil.redisKey(pageX, pageY, blockX, blockY);
        String cellLayout = CoordinateUtil.hashKey(cellX, cellY);
        redisTemplate.opsForHash().delete(blockLayout, cellLayout);
    }

    public static void makeCell(int pageX, int pageY, int blockX, int blockY, int cellX, int cellY, String color) {
        String blockLayout = CoordinateUtil.redisKey(pageX, pageY, blockX, blockY);
        String cellLayout = CoordinateUtil.hashKey(cellX, cellY);
        redisTemplate.opsForHash().put(blockLayout, cellLayout, color);
    }

    public static String takeCell(int pageX, int pageY, int blockX, int blockY, int cellX, int cellY) {
        String blockLayout = CoordinateUtil.redisKey(pageX, pageY, blockX, blockY);
        String cellLayout = CoordinateUtil.hashKey(cellX, cellY);
        return (String) redisTemplate.opsForHash().get(blockLayout, cellLayout);
    }
}
