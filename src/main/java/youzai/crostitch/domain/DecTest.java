package youzai.crostitch.domain;

import lombok.extern.slf4j.Slf4j;
import youzai.crostitch.Constants.Constants;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class DecTest implements ApplicationContextAware {
    private static ApplicationContext APP_CONTEXT = null;

    public static void main(String[] args) {
        System.out.println(new Date());
        String ss = "page0@0=block1@1";

        String[] as = ss.substring(ss.indexOf("k") + 1).split(Constants.COR_SPLIT);
        log.info(as.toString());
//        int[][] asd = {{1, 2,3,4}, {5,6,7,8},{9,10,11,12},{13,14,15,16}};

        Map<String, String> map = new HashMap<>();
        map.put("0@0", "00");
        map.put("0@1", "01");
        map.put("0@2", "02");
        map.put("1@0", "10");
        map.put("1@1", "11");
        map.put("1@2", "12");
        map.put("2@0", "20");
        map.put("2@1", "21");
        map.put("2@2", "22");

//        Object ss = map.entrySet().stream().sorted(Map.Entry.comparingByKey((k1, k2) -> {
//            String[] s1 = k1.split(Constants.COR_SPLIT);
//            String[] s2 = k2.split(Constants.COR_SPLIT);
//            for (int i = 0; i < 2; i++) {
//                if (Integer.parseInt(s1[i]) > Integer.parseInt(s2[i])) {
//                    return 1;
//                } else if (Integer.parseInt(s1[i]) < Integer.parseInt(s2[i])) {
//                    return -1;
//                }
//            }
//            return 0;
//        })).map(p -> {
//            int i = Integer.parseInt(p.getKey().split(Constants.COR_SPLIT)[0]);
//            String[] s = new String[3];
//            s[i] = p.getValue()
//            return String[]
//        }).toArray(String[]::new);

//        String[][] asd = new String[64][64];
//        map.entrySet().stream().map(r -> {
//            String[] ids = r.getKey().split(Constants.COR_SPLIT);
//            asd[Integer.parseInt(ids[0])][Integer.parseInt(ids[1])] = r.getValue();
//            return null;
//        });
//        log.info(map.toString());

//        for (int[] x : asd) {
//            for (int y : x) {
//                System.out.print(y);
//                System.out.print(",");
//            }
//            System.out.println("");
//        }
//        System.out.println(asd[1][1]);
//        System.out.println(String.valueOf(2) + "-" + String.valueOf(2));
//
//        Block bb = APP_CONTEXT.getBean(Block.class, 1, 1, 1, 1);
//        System.out.println(bb.toString());
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        APP_CONTEXT = applicationContext;
    }
}
