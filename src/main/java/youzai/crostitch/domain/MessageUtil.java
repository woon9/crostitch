package youzai.crostitch.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import youzai.crostitch.Utils.JsonUtil;
import youzai.crostitch.webSocket.CrosUserPool;
import youzai.crostitch.webSocket.CrowsSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wang
 */
@Component
@Slf4j
public class MessageUtil {
    @Autowired
    private CrosUserPool crosUserPool;

    @Autowired
    private CrowsSession crowsSession;

    public int castMessage(WebSocketSession session, TextMessage message) throws IOException {
        // 获得客户端传来的消息
        String payload = message.getPayload();
        Object token = session.getAttributes().get("token");
        log.info("server 接收到 {} 发送的 {}", token, payload);
        ConcurrentHashMap<String, WebSocketSession> sessions = crowsSession.getAll();
        TextMessage textMessage = new TextMessage("服务器返回 ->>> 【" + token + "】广播的消息： 【" + payload + "】  -  " + LocalDateTime.now().toString());
        for (WebSocketSession se : sessions.values()) {
            log.info("---服务器返回 ->>> [{}]广播给[{}]的消息： 【{}】 ", token, se.getAttributes().get("token"), payload);
            se.sendMessage(textMessage);
        }
        log.info("现在共有{}个fame，{}位用户", crosUserPool.getAll().size(), sessions.size());
        return sessions.values().size();
    }

    public int castMessage(TextMessage message) {
        // 获得客户端传来的消息
        String payload = message.getPayload();
        Map map = null;
        int count = 0;
        try {
            map = JsonUtil.json2map(payload);
            String type = (String) map.get("type");

            TextMessage textMessage = null;
            switch (type) {
                // 心跳检测
                case "heartCheck":
                    textMessage = new TextMessage(payload);
                    break;
                // 点击事件
                case "clickBlock":
                    textMessage = new TextMessage(payload);
                    this.doClick((ArrayList) map.get("content"));
                    break;
                // 改变Page背景色
                case "changeBgColor":
                    textMessage = new TextMessage(payload);
                    this.changePageColor((ArrayList<Integer>) map.get("page"), (String) map.get("content"),null);
                    break;
                // 改变Page边框色
                case "changeBdColor":
                    textMessage = new TextMessage(payload);
                    this.changePageColor((ArrayList<Integer>) map.get("page"), null, (String) map.get("content"));
                    break;
                // 增加区块
                case "addBlock":
                    textMessage = new TextMessage(payload);
                    int ct = this.createBlock((ArrayList<Integer>) map.get("page"), (ArrayList<ArrayList>) map.get("content"));
                    log.info("新增[{}]个区块，内容：[{}]",ct, payload);
                    break;
                // 减少区块
                case "reduceBlock":
                    textMessage = new TextMessage(payload);
                    int dt = this.deleteBlock((ArrayList<Integer>) map.get("page"), (ArrayList<ArrayList>) map.get("content"));
                    log.info("删除[{}]个区块，内容：[{}]",dt, payload);
                    break;
                default:
                    break;
            }
            ;
            if (textMessage != null) {
                log.info("消息类型：[{}]，广播内容:[{}]", type, JsonUtil.obj2json(map.get("content")));
                count = this.castToAll(textMessage);
            }
        } catch (JsonProcessingException e) {
            log.error("Payload json：{} 解析异常！", payload);
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("收到的消息[{}]已经广播给[{}]人", payload, count);
        return count;
    }

    private void doClick(ArrayList msg) {
        Integer pageX = (Integer) msg.get(0);
        Integer pageY = (Integer) msg.get(1);
        Integer blockX = (Integer) msg.get(2);
        Integer blockY = (Integer) msg.get(3);
        Integer x = (Integer) msg.get(4);
        Integer y = (Integer) msg.get(5);
        String color = (String) msg.get(6);
        CrosTool.makeCell(pageX, pageY, blockX, blockY, x, y, color);
    }

    private void changePageColor(ArrayList<Integer> layout, String bgColorNew, String bdColor) {
        CrosTool.changePageColor(layout.get(0), layout.get(1), bgColorNew, bdColor);
    }

    private int createBlock(ArrayList<Integer> layout, ArrayList<ArrayList> arrays) {

        int count = 0;
        int pageX = layout.get(0), pageY = layout.get(1), blockX, blockY;

        String[] colors = CrosTool.getPageColors(pageX, pageY);

        ArrayList<ArrayList> blocks = arrays.get(1);
        for (ArrayList block : blocks) {
            blockX = (Integer) block.get(0);
            blockY = (Integer) block.get(1);
            CrosTool.createBlock(pageX, pageY, blockX, blockY, colors[0], colors[1]);
            count++;
        }
        return count;
    }

    private int deleteBlock(ArrayList<Integer> layout, ArrayList<ArrayList> blocks){
        int count = 0;
        int pageX = layout.get(0), pageY = layout.get(1),blockX,blockY;
        for (ArrayList block : blocks) {
            blockX =  (Integer) block.get(0);
            blockY = (Integer) block.get(1);
            CrosTool.deleteBlock(pageX, pageY, blockX, blockY);
            count++;
        }
        return count;
    }

    private int castToAll(TextMessage textMessage) {
        Map<String, WebSocketSession> sessionMap = crowsSession.getAll();
        int count = 0;
        for (Object ws : sessionMap.values()) {
            WebSocketSession wsession = (WebSocketSession) ws;
            try {
                wsession.sendMessage(textMessage);
                count++;
            } catch (IOException e) {
                log.error("WebSocket sendMessage IO 异常！");
                log.error(e.getMessage());
                e.printStackTrace();
            }
        }
        return count;
    }

}
