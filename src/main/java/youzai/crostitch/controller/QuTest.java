//package myshow.crostitch.controller;
//
//import TestUtil;
//import myshow.crostitch.dao.Test2Mapper;
//import myshow.crostitch.dataModel.Test2;
//import myshow.crostitch.dataModel.Test2Example;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.context.ServletContextAware;
//
//import javax.servlet.ServletContext;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
////@EnableScheduling
//@RestController
//public class QuTest implements ServletContextAware {
//
//    private static int[][][][] test;
//
//    @Autowired
//    private Test2Mapper test2Mapper;
//
//    private Logger logger = LoggerFactory.getLogger(getClass());
//
//    @Override
//    public void setServletContext(ServletContext servletContext) {
//
//        test = new int[10][10][10][10];
//
//        Test2Example example = new Test2Example();
//        StringBuffer sort = new StringBuffer();
//        sort.append("INDEXX ASC");
//        sort.append(", INDEXY ASC");
//        example.setOrderByClause(sort.toString());
//        List<Test2> test2List = test2Mapper.selectByExample(example);
//
//        for (Test2 test2 : test2List) {
//            test[test2.getIndexx()][test2.getIndexy()][0] = TestUtil.getIntArrayFromCharArray(test2.getClum00().toCharArray());
//            test[test2.getIndexx()][test2.getIndexy()][1] = TestUtil.getIntArrayFromCharArray(test2.getClum01().toCharArray());
//            test[test2.getIndexx()][test2.getIndexy()][2] = TestUtil.getIntArrayFromCharArray(test2.getClum02().toCharArray());
//            test[test2.getIndexx()][test2.getIndexy()][3] = TestUtil.getIntArrayFromCharArray(test2.getClum03().toCharArray());
//            test[test2.getIndexx()][test2.getIndexy()][4] = TestUtil.getIntArrayFromCharArray(test2.getClum04().toCharArray());
//            test[test2.getIndexx()][test2.getIndexy()][5] = TestUtil.getIntArrayFromCharArray(test2.getClum05().toCharArray());
//            test[test2.getIndexx()][test2.getIndexy()][6] = TestUtil.getIntArrayFromCharArray(test2.getClum06().toCharArray());
//            test[test2.getIndexx()][test2.getIndexy()][7] = TestUtil.getIntArrayFromCharArray(test2.getClum07().toCharArray());
//            test[test2.getIndexx()][test2.getIndexy()][8] = TestUtil.getIntArrayFromCharArray(test2.getClum08().toCharArray());
//            test[test2.getIndexx()][test2.getIndexy()][9] = TestUtil.getIntArrayFromCharArray(test2.getClum09().toCharArray());
//        }
//    }
//
//    @Scheduled(cron = "0 0/3 * * * ?")
//    public void everyMin() {
//        logger.info("数据定期更新~");
//        for (int i = 0; i < 10; i++) {
//            for (int j = 0; j < 10; j++) {
//
//                Test2 test2 = new Test2();
//                test2.setClum00(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][0])));
//                test2.setClum01(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][1])));
//                test2.setClum02(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][2])));
//                test2.setClum03(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][3])));
//                test2.setClum04(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][4])));
//                test2.setClum05(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][5])));
//                test2.setClum06(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][6])));
//                test2.setClum07(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][7])));
//                test2.setClum08(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][8])));
//                test2.setClum09(String.valueOf(TestUtil.getCharArrayFromIntArray(test[i][j][9])));
//
//                Test2Example example = new Test2Example();
//                Test2Example.Criteria criteria = example.createCriteria();
//                criteria.andIndexxEqualTo(i);
//                criteria.andIndexyEqualTo(j);
//                test2Mapper.updateByExampleSelective(test2, example);
//            }
//        }
//    }
//
//    @RequestMapping(value = "/qu/init", method = RequestMethod.GET)
//    public Map<String, Object> init() {
//        logger.info("Hello Crostitch~");
//
//        Map<String, Object> map = new HashMap<String, Object>(1);
//        map.put("init", test);
//        logger.info("return init tab: {}", ((int[][][][]) map.get("init"))[0][0]);
//        return map;
//    }
//
//    @RequestMapping(value = "/qu/onClick", method = RequestMethod.GET)
//    public Map<String, Object> onClick(@RequestParam(name = "indexTabX", required = true) Integer indexTabX,
//                                       @RequestParam(name = "indexTabY", required = true) Integer indexTabY,
//                                       @RequestParam(name = "indexX", required = true) Integer indexX,
//                                       @RequestParam(name = "indexY", required = true) Integer indexY) {
//        test[indexTabX][indexTabY][indexX][indexY] = test[indexTabX][indexTabY][indexX][indexY] == 0 ? 1 : 0;
//        Map<String, Object> map = new HashMap<String, Object>(1);
//        map.put("tab", test[indexTabX][indexTabY]);
//        logger.info("[{},{}]", indexTabX, indexTabY);
//        logger.info("return tab: {}", (int[][]) map.get("tab"));
//        return map;
//    }
//}
