package youzai.crostitch.controller;

import lombok.extern.slf4j.Slf4j;
import youzai.crostitch.Constants.Constants;
import youzai.crostitch.Utils.JsonUtil;
import youzai.crostitch.domain.CrosTool;
import org.apache.lucene.util.RamUsageEstimator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@Slf4j
public class BlockController {

    @PostMapping(value = "/ask/getPage")
    public Map<String, Object> getPage(@RequestBody Map<String, Integer> params) {
        int pageX = params.get("pageX") == null ? 0 : params.get("pageX");
        int pageY = params.get("pageY") == null ? 0 : params.get("pageY");
        Map<String, Object> map = new ConcurrentHashMap<>();
        map.put(Constants.SITE, new int[]{pageX, pageY});

        Map pageMap = CrosTool.getPage(pageX, pageY);
        if (pageMap.size() > 0) {
            map.put(Constants.STATE, "200");
            map.put(Constants.MESSAGE, Constants.SUCCESS);
            map.put(Constants.BG_COLOR, pageMap.get(Constants.BG_COLOR));
            map.put(Constants.BD_COLOR, pageMap.get(Constants.BD_COLOR));
            pageMap.remove(Constants.BG_COLOR);
            pageMap.remove(Constants.BD_COLOR);
            map.put(Constants.PAGE, pageMap);
            log.info("------获取页[{},{}]成功", pageX, pageY);
        } else {
            map.put(Constants.STATE, "400");
            map.put(Constants.MESSAGE, Constants.ERROR);
            map.put(Constants.MESSAGE, "目前没有当前页！");
            log.info("------获取页[{},{}]失败，目前没有当前页！", pageX, pageY);
        }
        return map;
    }

    @PostMapping(value = "/ask/createPage")
    public Map<String, Object> createPage(@RequestBody Map<String, Integer> params) {
        int pageX = params.get("pageX") == null ? 0 : params.get("pageX");
        int pageY = params.get("pageY") == null ? 0 : params.get("pageY");
        Map<String, Object> map = new ConcurrentHashMap<>();
        map.put(Constants.SITE, new int[]{pageX, pageY});
        if (CrosTool.createPage(pageX, pageY)) {
            map.put(Constants.STATE, "200");
            map.put(Constants.MESSAGE, Constants.SUCCESS);
            log.info("------创建页[{},{}]成功", pageX, pageY);
        } else {
            map.put(Constants.STATE, "400");
            map.put(Constants.MESSAGE, Constants.ERROR);
            map.put(Constants.MESSAGE, "创建失败，当前页已存在！");
            log.info("------创建页[{},{}]失败，当前页已存在！", pageX, pageY);
        }
        return map;
    }

    @PostMapping(value = "/ask/getBlock")
    public Map<String, Object> getBlock(@RequestBody Map<String, Integer> params) {
        int pageX = params.get("pageX") == null ? 0 : params.get("pageX");
        int pageY = params.get("pageY") == null ? 0 : params.get("pageY");
        int blockX = params.get("blockX") == null ? 0 : params.get("blockX");
        int blockY = params.get("blockY") == null ? 0 : params.get("blockY");
        Map<String, Object> map = new ConcurrentHashMap<>();
        map.put(Constants.SITE, new int[]{pageX, pageY, blockX, blockY});

        if (CrosTool.hasBlock(pageX, pageY, blockX, blockY)) {
            map.put(Constants.STATE, "200");
            map.put(Constants.MESSAGE, Constants.SUCCESS);
            map.put(Constants.BLOCK, CrosTool.getBlock(pageX, pageY, blockX, blockY));
            map.put(Constants.BG_COLOR, CrosTool.getBlockColors(pageX, pageY, blockX, blockY)[0]);
            map.put(Constants.BD_COLOR, CrosTool.getBlockColors(pageX, pageY, blockX, blockY)[1]);
            log.info("------获取区块[{},{} - {},{}]成功", pageX, pageY, blockX, blockY);
        } else {
            map.put(Constants.STATE, "400");
            map.put(Constants.MESSAGE, Constants.ERROR);
            map.put(Constants.MESSAGE, "目前没有当前区块！");
            log.info("------获取区块[{},{} - {},{}]失败，目前没有当前区块！", pageX, pageY, blockX, blockY);
        }

        return map;
    }

    @PostMapping(value = "/ask/createBlock")
    public Map<String, Object> createBlock(@RequestBody Map<String, Integer> params) {
        int pageX = params.get("pageX") == null ? 0 : params.get("pageX");
        int pageY = params.get("pageY") == null ? 0 : params.get("pageY");
        int blockX = params.get("blockX") == null ? 0 : params.get("blockX");
        int blockY = params.get("blockY") == null ? 0 : params.get("blockY");

        Map<String, Object> map = new ConcurrentHashMap<>();
        map.put(Constants.SITE, new int[]{pageX, pageY, blockX, blockY});

        if (CrosTool.createBlock(pageX, pageY, blockX, blockY)) {
            map.put(Constants.STATE, "200");
            map.put(Constants.MESSAGE, Constants.SUCCESS);
            map.put(Constants.BLOCK, CrosTool.getBlock(pageX, pageY, blockX, blockY));
            log.info("------>>>>>>创建区块[{},{} - {},{}]成功", pageX, pageY, blockX, blockY);
        } else {
            map.put(Constants.STATE, "500");
            map.put(Constants.MESSAGE, Constants.ERROR);
            map.put(Constants.MESSAGE, "创建失败，当前区块已存在！");
            log.info("------>>>>>>创建区块[{},{} - {},{}]失败，当前区块已存在！", pageX, pageY, blockX, blockY);
        }

        return map;
    }

    @PostMapping(value = "/ask/getColors")
    public Map<String, Object> getColors(@RequestBody Map<String, Integer> params) {
        int pageX = params.get("pageX") == null ? 0 : params.get("pageX");
        int pageY = params.get("pageY") == null ? 0 : params.get("pageY");
        int blockX = params.get("blockX") == null ? -1 : params.get("blockX");
        int blockY = params.get("blockY") == null ? -1 : params.get("blockY");
        Map<String, Object> map = new ConcurrentHashMap<>();
        if (blockX > 0 && blockY > 0) {
            map.put(Constants.SITE, new int[]{pageX, pageY, blockX, blockY});
        } else {
            map.put(Constants.SITE, new int[]{pageX, pageY});
        }
        try {
            map.put(Constants.BG_COLOR, Constants.BG_COLOR_INIT_VALUE);
            map.put(Constants.BD_COLOR, Constants.BD_COLOR_INIT_VALUE);
            String[] colors = CrosTool.getPageColors(pageX, pageY);
            if (colors != null) {
                map.put(Constants.BG_COLOR, colors[0]);
                map.put(Constants.BD_COLOR, colors[1]);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return map;
    }

    @PostMapping(value = "/ask/getInitInfo")
    public Map<String, Object> testPrint(@RequestBody Map<String, Integer> params) {
        int pageX = params.get("pageX") == null ? 0 : params.get("pageX");
        int pageY = params.get("pageY") == null ? 0 : params.get("pageY");
        int blockX = params.get("blockX") == null ? -1 : params.get("blockX");
        int blockY = params.get("blockY") == null ? -1 : params.get("blockY");
        Map<String, Object> map = new ConcurrentHashMap<>();
        if (blockX > 0 && blockY > 0) {
            map.put(Constants.SITE, new int[]{pageX, pageY, blockX, blockY});
        } else {
            map.put(Constants.SITE, new int[]{pageX, pageY});
        }
        try {
            Map pageMap = CrosTool.getPage(pageX, pageY);
            map.put(Constants.BG_COLOR, Constants.BG_COLOR_INIT_VALUE);
            map.put(Constants.BD_COLOR, Constants.BD_COLOR_INIT_VALUE);
            if (!pageMap.isEmpty()) {
                map.put(Constants.BG_COLOR, pageMap.get(Constants.BG_COLOR));
                map.put(Constants.BD_COLOR, pageMap.get(Constants.BD_COLOR));
                long size = RamUsageEstimator.sizeOf(JsonUtil.mapToJson(pageMap));
                map.put(Constants.RESPONSE_DATA_SIZE, String.valueOf(size));
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return map;
    }
}
