package youzai.crostitch.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author wang
 */
@Slf4j
@Component
public class EnvironmentConfig implements BeanFactoryPostProcessor, BeanPostProcessor, EnvironmentAware {
    private static String OS, LOCALHOST = "127.0.0.1";
//    private static Integer LOCALPORT = 8081;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        //windows
        EnvironmentConfig.OS = (String) ((Map) beanFactory.getBean("systemProperties")).get("sun.desktop");
//        Object aaa = ((Map)((OriginTrackedMapPropertySource)((List)((MutablePropertySources)((StandardServletEnvironment)beanFactory.getBean("environment")).propertySources).propertySourceList).get(6)).source);//.put("spring.redis.host","127.0.0.1")
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    /**
     * server-org.springframework.boot.autoconfigure.web.ServerProperties
     * spring.redis-org.springframework.boot.autoconfigure.data.redis.RedisProperties
     */

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (!OS.toLowerCase().startsWith("win")) {
            if (beanName.contains("RedisProperties") && bean instanceof RedisProperties) {
                RedisProperties redisProperties = (RedisProperties) bean;
                redisProperties.setHost(LOCALHOST);
                log.info("------设置redis host:{}", LOCALHOST);
            }
//            else if (beanName.contains("ServerProperties") && bean instanceof ServerProperties) {
//                ServerProperties serverProperties = (ServerProperties) bean;
//                serverProperties.setPort(LOCALPORT);
//                log.info("------设置server port:{}",LOCALPORT);
//            }

        }

        return bean;
    }

    @Override
    public void setEnvironment(Environment environment) {
        String host = environment.resolveRequiredPlaceholders("${spring.redis.host}");
        log.info("---yml中配置的host是：{}",host);
    }
}
