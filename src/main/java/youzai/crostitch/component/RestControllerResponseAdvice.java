package youzai.crostitch.component;

import lombok.extern.slf4j.Slf4j;
import youzai.crostitch.Constants.Constants;
import youzai.crostitch.Utils.JsonUtil;
import org.apache.lucene.util.RamUsageEstimator;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Map;

@ControllerAdvice
@Slf4j
public class RestControllerResponseAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
                                  ServerHttpResponse response) {
        if (body instanceof Map) {
            Map bodyMap = (Map) body;
            long size;
            if (bodyMap.containsKey(Constants.PAGE)) {
                size = RamUsageEstimator.sizeOf(JsonUtil.mapToJson((Map) ((Map<?, ?>) body).get(Constants.PAGE)));
            } else if (bodyMap.containsKey(Constants.PAGE)) {
                size = RamUsageEstimator.sizeOf(JsonUtil.mapToJson((Map) ((Map<?, ?>) body).get(Constants.BLOCK)));
            } else {
                size = RamUsageEstimator.sizeOf(JsonUtil.mapToJson(bodyMap));
            }
            response.getHeaders().add(Constants.RESPONSE_DATA_SIZE, String.valueOf(size));
            log.info("返回对[{}]的请求，大小约为[{}]", request.getURI(), size);
        }
        return body;
    }
}
