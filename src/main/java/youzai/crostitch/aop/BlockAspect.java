package youzai.crostitch.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class BlockAspect {

//    @Before("@annotation(BlockRecord)")
//    public void beforeBlock(JoinPoint pjp) {
//        log.info("---beforeBlock");
//    }

    @After("@annotation(BlockRecord)")
    public void afterBlock(JoinPoint pjp) {
        log.info("AOP---afterBlock");
    }


//    @Around("@annotation(BlockRecord)")
//    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
//        try {
//            log.info("---around before");
//            return joinPoint.proceed();
//        } catch (Throwable e) {
//            e.printStackTrace();
//            throw e;
//        } finally {
//            log.info("---around after");
//        }
//    }

//    @AfterThrowing("@annotation(BlockRecord)")
//    public void afterThrowing() {
//        log.info("afterThrowing afterThrowing  rollback");
//    }


//    @AfterReturning(pointcut = "@annotation(BlockRecord)", argNames = "returnObject", returning = "returnObject")
//    public void afterReturning(JoinPoint joinPoint, Object returnObject) {
//        log.info("---afterReturning");
//    }
}
