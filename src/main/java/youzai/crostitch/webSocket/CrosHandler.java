package youzai.crostitch.webSocket;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import youzai.crostitch.domain.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * 通过继承  TextWebSocketHandler 类并覆盖相应方法，可以对 websocket 的事件进行处理，这里可以同原生注解的那几个注解连起来看
 * <p>
 * afterConnectionEstablished  方法是在 socket 连接成功后被触发，同原生注解里的 @OnOpen 功能
 * afterConnectionClosed  **方法是在 socket 连接关闭后被触发，同原生注解里的 @OnClose 功能
 * handleTextMessage **方法是在客户端发送信息时触发，同原生注解里的  @OnMessage 功能
 */
@Component
@Slf4j
public class CrosHandler extends TextWebSocketHandler {
    @Autowired
    private CrosUserPool crosUserPool;

    @Autowired
    private CrowsSession crowsSession;

    @Autowired
    private MessageUtil messageUtil;


    /**
     * socket 建立成功事件
     *
     * @param session
     * @throws Exception
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String token = (String) session.getAttributes().get("token");
        crowsSession.add(token, session);
        String fame = (String) session.getAttributes().get("fame");
        crosUserPool.add(fame, token, session);
        log.info("现有用户共{}人，其中{}用户组现有{}成员！", crowsSession.getAll().size(), fame, crosUserPool.get(fame).size());
    }

    /**
     * socket 断开连接时
     *
     * @param session
     * @param status
     * @throws Exception
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        String token = (String) session.getAttributes().get("token");
        if (StrUtil.isNotBlank(token)) {
            // 用户退出，移除缓存
            crowsSession.remove(token);
        }

        String fame = (String) session.getAttributes().get("fame");
        if (StrUtil.isNotBlank(fame)) {
            // 用户退出，移除缓存
            crosUserPool.remove(fame, token);
        }

        log.info("用户[{}]退出，现在系统共有{}名用户，其中[{}]fame还有{}人", token, crowsSession.getAll().size(), fame, crosUserPool.get(fame) == null ? 0 : crosUserPool.get(fame).size());
    }

    /**
     * 接收消息事件
     *
     * @param session
     * @param message
     * @throws Exception
     */
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        messageUtil.castMessage(message);
    }

}
