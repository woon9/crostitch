package youzai.crostitch.webSocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * 说明
 * 通过实现  WebSocketConfigurer 类并覆盖相应的方法进行 websocket 的配置。
 * 我们主要覆盖  registerWebSocketHandlers 这个方法。
 * 通过向  WebSocketHandlerRegistry 设置不同参数来进行配置。
 * 其中 **addHandler **方法添加我们上面的写的 ws 的   handler 处理类，
 * 第二个参数是你暴露出的 ws 路径。**addInterceptors **添加我们写的握手过滤器。
 * **setAllowedOrigins("*") **这个是关闭跨域校验，方便本地调试，线上推荐打开。
 */
@Configuration
@EnableWebSocket
@Slf4j
public class WebSocketConfig implements WebSocketConfigurer {
    public static String WS_PATH = "crows";

    @Autowired
    private CrosHandler crosHandler;
    @Autowired
    private CrosInterceptor crosInterceptor;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry
                .addHandler(crosHandler, WS_PATH)
                .addInterceptors(crosInterceptor)
                .setAllowedOrigins("*");
        // ws://127.0.0.1:8081/crows?fame=dawang&token=theToken
        log.info("==========================注册WebSocketConfig，Path：【{}】",WS_PATH);
        log.info("------>>>测试用：{}<<<------","ws://127.0.0.1:8081/crows?fame=dawang&token=theToken");
    }
}
