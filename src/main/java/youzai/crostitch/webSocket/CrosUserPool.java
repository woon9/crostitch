package youzai.crostitch.webSocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 说明
 * 这里简单通过  **ConcurrentHashMap **来实现了一个 session 池，用来保存已经登录的 web socket 的   session。
 * 前文提过，服务端发送消息给客户端必须要通过这个 session。
 */
@Slf4j
@Component
public class CrosUserPool {
    /**
     * 保存连接 session 的地方
     */
    private ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> SESSION_POOL = new ConcurrentHashMap<>();

    public void add(String fame, String token, WebSocketSession session) {
        if (SESSION_POOL.containsKey(fame)) {
            SESSION_POOL.get(fame).put(token, session);
        } else {
            ConcurrentHashMap<String, WebSocketSession> webSocketSessionConcurrentHashMap = new ConcurrentHashMap();
            webSocketSessionConcurrentHashMap.put(token, session);
            SESSION_POOL.put(fame, webSocketSessionConcurrentHashMap);
        }
    }

    public ConcurrentHashMap<String, WebSocketSession> remove(String fame) {
        return SESSION_POOL.remove(fame);
    }

    public WebSocketSession remove(String fame, String token) {
        if (SESSION_POOL.containsKey(fame) && SESSION_POOL.get(fame).containsKey(token)) {
            WebSocketSession wss = SESSION_POOL.get(fame).remove(token);
            if (SESSION_POOL.get(fame).size() == 0) {
                SESSION_POOL.remove(fame);
            }
            return wss;
        }
        return null;
    }

    public void removeAndClose(String fame) {
        ConcurrentHashMap<String, WebSocketSession> sessions = remove(fame);
        for (WebSocketSession session : sessions.values()) {
            if (session != null) {
                try {
                    // 关闭连接
                    session.close();
                } catch (IOException e) {
                    // todo: 关闭出现异常处理
                    e.printStackTrace();
                }
            }
        }
    }

    public ConcurrentHashMap<String, WebSocketSession> get(String fame) {
        return SESSION_POOL.get(fame);
    }

    public ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>>  getAll() {
        return SESSION_POOL;
    }
}
