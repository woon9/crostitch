package youzai.crostitch.Utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @author wang
 */
@Component
public class SpringContextUtil implements ApplicationContextAware {
    private static ApplicationContext APP_CONTEXT = null;

    public static ApplicationContext getAppContext() {
        return APP_CONTEXT;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        APP_CONTEXT = applicationContext;
    }

    public static <T> T getBean(Class<T> requiredType) {
        return APP_CONTEXT.getBean(requiredType);
    }

    public static <T> T getBean(String name, Class<T> requiredType) {
        return APP_CONTEXT.getBean(name, requiredType);
    }

    public static Object getBean(String name) {
        return APP_CONTEXT.getBean(name);
    }

    public static Object getBean(String name, Object... args) {
        return APP_CONTEXT.getBean(name, args);
    }

    public static boolean contains(String name) {
        return APP_CONTEXT.containsBean(name);
    }

    public static boolean isSignleton(String name) {
        return APP_CONTEXT.isSingleton(name);
    }

    public static Class<?> getType(String name) {
        return APP_CONTEXT.getType(name);
    }

    public static String[] getAliases(String name) {
        return APP_CONTEXT.getAliases(name);
    }

    public String[] getBeanNamesForType(Class<?> clazz) {
        return APP_CONTEXT.getBeanNamesForType(clazz);
    }
}
