package youzai.crostitch.Utils;

public class TestUtil {

    public static int[] getIntArrayFromCharArray(char[] charArray) {
        int[] intArray = new int[charArray.length];
        for (int i = 0; i < charArray.length; i++) {
            intArray[i] = (int) charArray[i] - (int) ('0');
        }
        return intArray;
    }

    public static char[] getCharArrayFromIntArray(int[] intArray) {

        char[] charArray = new char[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            charArray[i] = (char) (intArray[i] + (int) ('0'));
        }
        return charArray;
    }
}
