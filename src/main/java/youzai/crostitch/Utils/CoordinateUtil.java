package youzai.crostitch.Utils;

import youzai.crostitch.Constants.Constants;
import org.springframework.util.Assert;
//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

import java.io.IOException;

public class CoordinateUtil {
    public static  String int2Str(int x,int y) {
        return String.valueOf(x) + Constants.COR_SPLIT + String.valueOf(y);
    }

    public static String int4Str(int... args) {
        StringBuffer sb = new StringBuffer();
        for (int i : args) {
            sb.append(i).append(Constants.COR_SPLIT);
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }


    public static String hashKey(int x, int y) {
        return String.valueOf(x) + Constants.COR_SPLIT + String.valueOf(y);
    }

    public static String redisKey(int... args) {
        Assert.state(args != null && (args.length == 2 || args.length == 4), "转换redisKey的参数必须为2位或者4位！");
        StringBuffer sb = new StringBuffer();
        sb.append("Page");
        for (int i = 0; i < args.length; i++) {
            if (i == 2) {
                sb.setLength(sb.length()-1);
                sb.append("=Block");
            }
            sb.append(args[i]).append(Constants.COR_SPLIT);
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }
//
//    /**
//     * 二进制流转Base64字符串
//     *
//     * @param data 二进制流
//     * @return data
//     * @throws IOException 异常
//     */
//    public static String getBase64String(byte[] data) throws IOException {
//        BASE64Encoder encoder = new BASE64Encoder();
//        return data != null ? encoder.encode(data) : "";
//    }
//
//    /**
//     * Base64字符串转 二进制流
//     *
//     * @param base64String Base64
//     * @return base64String
//     * @throws IOException 异常
//     */
//    public static byte[] getStringBase64(String base64String) throws IOException {
//        BASE64Decoder decoder = new sun.misc.BASE64Decoder();
//        return base64String != null ? decoder.decodeBuffer(base64String) : null;
//    }
}
