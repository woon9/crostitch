package youzai.crostitch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrostitchApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrostitchApplication.class, args);
	}
//	Disruptor dr = new Disruptor();
}
