package youzai.crostitch.Constants;

public class Constants {
    public static String COR_SPLIT = "@";

    public static String STATE = "state";

    public static String MESSAGE = "msg";

    public static String SUCCESS = "success";

    public static String ERROR = "error";

    public static String PAGE = "page";

    public static String BLOCK = "block";

    public static String BLOCKS = "blocks";

    public static String SITE = "site";

    public static String REDIS_KEY = "Crostitch-Pool-1";

    public static String BG_COLOR = "bgColor";

    public static String BD_COLOR = "bdColor";

    public static String RESPONSE_DATA_SIZE = "dataSize";

    public static String BG_COLOR_INIT_VALUE = "#D3D3D3";

    public static String BD_COLOR_INIT_VALUE = "#808080";

    public static double REDIS_ZSET_SCORE = 999.999;

    public static int PAGE_SIZE = 64;

    public static int PAGE_ROW = 1;

    public static int PAGE_COL = 1;

    public static int BLOCK_ROW = 3;

    public static int BLOCK_COL = 3;

    public static int CELL_ROW = 64;

    public static int CELL_COL = 64;
}
