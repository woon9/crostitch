package youzai.crostitch;


import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

@Slf4j
public class wQueueTest {
    private static int cellVolume = 200, cellCount = 100, gap = 1;
    private static long x = 20000, y = 5000, t = 1, r = 50, n1 = 0,
            stopCount = (t * y * x - cellVolume), probe1, probe2, probe3, probe0;
    private static volatile long start1, end1, start2, end2, start3, end3, start0, end0;

    private static volatile boolean err = false;

    public static void main(String[] args) throws InterruptedException {
        probe1 = 0L;
        probe3 = 0L;
        probe2 = 0L;
        probe0 = 0L;

        wQueue<Long> queueT = new wQueue<>(cellCount, cellVolume, gap);
        Thread Tread1 = new Thread(() -> {
            Thread.currentThread().setName("Thread-read1");
            start1 = System.nanoTime();
            try {
                queueT.poll(data -> {
                    if (err) {
                        return false;
                    }
                    if (data != probe1++) {
                        err = true;
                        log.info("err!!!err!!!err!!!err!!!err!!!");
                        return false;
                    }
                    if (probe1 > stopCount)
                        return false;
                    long s1 = System.nanoTime();

//                    while (System.nanoTime() - s1 < 3_500) {}

//                    if (probe1 % 10_000 == 0 && probe1 > 9_999)
//                        log.info("probe1:" + probe1);
                    return true;
                });
            } catch (InterruptedException e) {
                log.warn("Queue read线程被打断:{}->{}", e.getMessage(), Arrays.toString(e.getStackTrace()));
            }
            end1 = System.nanoTime();
        });
        Tread1.start();

//        Thread Tread2 = new Thread(() -> {
//            Thread.currentThread().setName("Thread-read2");
//            start2 = System.nanoTime();
//            try {
//                queueT.poll(data -> {
//                    if (err) {
//                        return false;
//                    }
//                    if (data != probe2++) {
//                        err = true;
//                        log.info("err!!!err!!!err!!!err!!!err!!!");
//                        return false;
//                    }
//                    if (probe2 > stopCount)
//                        return false;
////                    if (probe2 % 10_000 == 0 && probe2 > 9_999)
////                        log.info("probe2:" + probe2);
//                    return true;
//                });
//            } catch (InterruptedException e) {
//                log.warn("Queue read线程被打断:{}->{}", e.getMessage(), Arrays.toString(e.getStackTrace()));
//            }
//            end2 = System.nanoTime();
//        });
//        Tread2.start();
//
//        Thread Tread3 = new Thread(() -> {
//            Thread.currentThread().setName("Thread-read3");
//            start3 = System.nanoTime();
//            try {
//                queueT.poll(data -> {
//                    if (err) {
//                        return false;
//                    }
//                    if (data != probe3++) {
//                        err = true;
//                        log.info("err!!!err!!!err!!!err!!!err!!!");
//                        return false;
//                    }
//                    if (probe3 > stopCount)
//                        return false;
////                    if (probe3 % 10_000 == 0 && probe3 > 9_999)
////                        log.info("probe3:" + probe3);
//                    return true;
//                });
//            } catch (InterruptedException e) {
//                log.warn("Queue read线程被打断:{}->{}", e.getMessage(), Arrays.toString(e.getStackTrace()));
//            }
//            end3 = System.nanoTime();
//        });
//        Tread3.start();

//        while (queueT.countConsumer() < 3) {
//        }

        log.info("注册消费者:" + queueT.countConsumer());

        Thread Twrite = new Thread(() -> {
            Thread.currentThread().setName("Thread-write");
            start0 = System.nanoTime();
            for (int d = 0; d < t; d++) {
                try {
                    for (int i = 0; i < x; i++) {
                        for (int j = 0; j < y; j++) {
                            queueT.offer(probe0++);
                        }
                    }
//                    queueT.squeeze();
//                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    log.info("writer被打断了了了了了了");
                }
            }
            end0 = System.nanoTime();
            if (end0 < start0)
                System.out.println("--->W-n=" + n1);

        });
        Twrite.start();

        Tread1.join();
//        Tread2.join();
//        Tread3.join();

        Twrite.join(200);

        if (Twrite.isAlive())
            Twrite.interrupt();

        if (end0 < start0)
            System.out.println("--->Main-n=" + n1);

        if (!err) {
            log.info("第" + ++n1 + "次读取成功结束，验证无误。"
                    + "\r\nlist1共读取" + probe1 / 100_000_000.000 + "亿条数据，耗时:"
                    + ((end1 - start1) / 1000_000_000.000) + "s"
                    + "\r\nlist2共读取" + probe2 / 100_000_000.000 + "亿条数据，耗时:"
                    + ((end2 - start2) / 1000_000_000.000) + "s"
                    + "\r\nlist3共读取" + probe3 / 100_000_000.000 + "亿条数据，耗时:"
                    + ((end3 - start3) / 1000_000_000.000) + "s"
                    + "\r\nwriter共写入" + probe0 / 100_000_000.000 + "亿条数据，耗时:"
                    + ((end0 - start0) / 1000_000_000.000) + "s");

            if (n1 < r) {
                wQueueTest.main(null);
            }
            log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        } else {
            log.info("Err----------------------");
        }
    }
}
