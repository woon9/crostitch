package youzai.crostitch;

import cn.hutool.core.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;


import java.util.HashMap;
import java.util.Map;

@Slf4j
@EnableAutoConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CrostitchApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void getInitInfo() {
		Map pram = new HashMap();
		pram.put("pageX",0);
		pram.put("pageY",0);
		Object obj = restTemplate.postForEntity("http://crostitch.youzai.plus/ask/getInitInfo",pram,Map.class);
		String bgColor = ((Map) ((ResponseEntity) obj).getBody()).get("bgColor").toString();
		Assert.state("#006400".equals(bgColor),"bgColor不是#006400！");
	}

//	@Test
//	void contextLoads() {
//
//	}

}
